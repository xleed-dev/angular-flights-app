import { TypeOrmModule } from '@nestjs/typeorm/dist/typeorm.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Flights } from './flights/flight.entity';
import { FlightsModule } from './flights/flights.module';

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'postgres',
    password: 'postgres',
    database: 'transportation',
    entities: [Flights],
    synchronize: true,
  }),FlightsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
