import { Injectable } from '@angular/core';
import { Flight } from './flight.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FlightsService {

  private apiURL = 'http://127.0.0.1:3000/flights';

  constructor(private http: HttpClient) { }

  getFlights(orig: string, dest: string) {
    const queryURL = `${this.apiURL}/query/${orig}/${dest}`;
    return this.http.get<Flight[]>(queryURL);
  }

  getAllFlights() {
    return this.http.get<Flight[]>(this.apiURL);
  }

  postFlight(flight: Flight) {
    return this.http.post(this.apiURL,flight);
  }

  updateFlight(flight: Flight) {
    return this.http.patch(`${this.apiURL}/${flight.id}/update`, flight);
  }

  deleteFlight(id: number) {
    return this.http.delete(`${this.apiURL}/${id}/delete`);
  }

  getAllOrigins(): Observable<any> {
    return this.http.get(`${this.apiURL}/cities/origins`)
  }

  getAllDestinations(): Observable<any> {
    return this.http.get(`${this.apiURL}/cities/destinations`)
  }

}
