import { FlightsService } from './../flights.service';
import { Flight } from './../flight.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  flightList: Flight[];
  messages: string;
  errors: string;

  origin: string;
  destination: string;
  flightNumber: number;
  depart: Date;
  arrive: Date;
  nonstop: boolean = false;

  constructor(private flightsService: FlightsService) { }

  toggleNonStop() {
    this.nonstop = !this.nonstop;
  }

  loadData() {
    this.flightsService.getAllFlights().subscribe(data => {
      console.log(data);      
      this.flightList = data;
    })
  } 

  ngOnInit(): void {
    this.loadData();
  }

  sendFlight() {
    const flight: Flight = {
      origin: this.origin,
      destination: this.destination,
      flightNumber: this.flightNumber,
      depart: this.depart,
      arrive: this.arrive,
      nonstop: this.nonstop
    }
    this.flightsService.postFlight(flight).subscribe(
      (result) => {this.messages = "Success";this.loadData()},
      (error) => {this.errors = JSON.stringify(error);}
    );

  }

  update(flight: Flight) {
    this.flightsService.updateFlight(flight).subscribe(data => {
      if (data && data['affected']) {
        this.loadData();
      }
    });
  }
  
  delete(flight: Flight) {
    if (!window.confirm('are you sure you want to delete this flight? ')) {
      return; 
    } 
    this.flightsService.deleteFlight(flight.id).subscribe(
      data => {
        if (data && data['affected']) {
          this.loadData();
          this.messages = flight.id+" deleted";
      }
    },
    error => {
        console.log("error deleting ",flight);
        console.log(error);
    });
  } 

}
