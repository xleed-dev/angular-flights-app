import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Flight } from '../flight.model';
import { FlightsService } from "../flights.service";
import "reflect-metadata";
import { error } from 'protractor';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  
  flights: Flight[];
  errors: string;
  messages: string;
  selectedOrigin: string;
  selectedDestiny: string;
  filteredOriginList: string;
  filteredDestinationList: string;
  
  constructor(private flightsService : FlightsService) {
    
  }
  
  ngOnInit(): void {
    this.flightsService.getAllOrigins()
    .subscribe(
      (data) => {this.filteredOriginList=data;this.errors=null},
      (errors) => {this.errors = JSON.stringify(errors);}
    )
    this.flightsService.getAllDestinations()
    .subscribe(
      (data) => { this.filteredDestinationList = data; this.errors = null},
      (errors) => {this.errors = JSON.stringify(errors);}
    )
  }
    
  query(): void {
    const destiny = this.selectedDestiny;
    const origin = this.selectedOrigin;

    this.flightsService.getFlights(origin,destiny)
    .subscribe(
      (results) => { 
        this.flights = results; 
        if(results==null || results.length===0) {
          this.messages = "No results";
        }
        else {
          this.errors = null;
          this.messages = null;
        }
      },
      (error) => {console.log(error); this.errors = error.message;},
      () => { console.log('fetch done'); }
    );
  }
    
}
  