import { Flights } from './flight.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';

@Injectable()
export class FlightsService {
    
    constructor(
        @InjectRepository(Flights) 
        private readonly flightRepostory: Repository<Flights>
    ) {}

    async findAll(): Promise<Flights[]> {
        return this.flightRepostory.find();
    }

    async findOne(id: number): Promise<any> {
        return this.flightRepostory.findOne(id);
    } 

    async query(orig: string, dest: string) {
        return await this.flightRepostory.find({origin: orig, destination: dest});
    }

    async create(flight: Flights): Promise<any> {
        return await this.flightRepostory.save(flight);
    }

    async update(flight: Flights): Promise<UpdateResult> {
        return await this.flightRepostory.update(flight.id,flight);
    }

    async delete(id: number): Promise<DeleteResult> {
        return await this.flightRepostory.delete(id);
    }

    async getFlightOrigins(): Promise<string[]> {
        return this.flightRepostory.query("select distinct origin from flights");
    }

    async getFlightDestinations(): Promise<string[]> {
        return this.flightRepostory.query("select distinct destination from flights");
    }

}
